;;; init-c.el --- C/C++ editing  -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(setq c-default-style "k&r"
      c-basic-offset 4)

;; Package: clean-aindent-mode
;; (require 'clean-aindent-mode)
;; (add-hook 'prog-mode-hook 'clean-aindent-mode)


(setq path-to-ctags "/usr/bin/ctags")

(defun create-tags (dir-name)
  "Create tags file in path DIR-NAME."
  (interactive "DDirectory: ")
  (shell-command
   (format "%s -f TAGS -e -R %s" path-to-ctags (directory-file-name dir-name))))

(provide 'init-c)
;;; init-c.el ends here
