;;; init-latex.el --- Provide LaTeX functions  -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require-package 'auctex)
(require 'company)
(require 'company-auctex)
(company-auctex-init)
;; Turn on RefTeX in AUCTeX
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)

;; Activate nice interface between RefTeX and AUCTeX
(setq reftex-plug-into-AUCTeX t)
(setq TeX-parse-self t)
(setq TeX-auto-save t)

;; Make LaTeX and RefTeX aware of math environments
(setq reftex-label-alist
      '(
	("definition" ?d "def:" "~\\cref{%s}" t  ("definition"))
	("theorem" ?t "thm:" "~\\cref{%s}" t  ("theorem" "th."))
	("proof" ?p "proof:" "~\\cref{%s}" t  ("proof" "pf."))
	("proposition" ?P "prop:" "~\\cref{%s}" t  ("proposition" "prop."))
	("lemma" ?l "lem:" "~\\cref{%s}" t  ("lemma"))
	("corollary" ?c "cor:" "~\\cref{%s}" t  ("corollary" "cor."))
	("example" ?x "example:" "~\\cref{%s}" t  ("example" "ex."))
	("remark" ?r "remark:" "~\\cref{%s}" t  ("remark" "rmk."))))

;; (defcustom LaTeX-theorem-label "thm:"
;;   "*Default prefix to theorem labels."
;;   :group 'LaTeX-label
;;   :group 'LaTeX-environment
;;   :type 'string)
;; (add-to-list 'LaTeX-label-alist
;;	     '("theorem" . LaTeX-theorem-label))

;; For helping AucTeX
(setq TeX-save-query nil)
(setq TeX-PDF-mode t)
(setq-default TeX-master nil)

(defun my-latex-mode-config ()
  "For use in `latex-mode-hook'."
  (local-set-key (kbd "<f9>") 'insert-subscript) ; add a key
					;  (local-set-key (kbd "C-c )") 'reftex-cleveref-cref)
  (setq auto-complete-mode t)
  (TeX-add-symbols "text")
  (LaTeX-math-mode)
  (auto-fill-mode)
  (TeX-source-correlate-mode t)
  (setq TeX-source-correlate-start-server t)
					;  (setq reftex-label-alist '(AMSTeX))	; Remove () around refs
  ;; more here
  )

;; add to hook
(add-hook 'LaTeX-mode-hook 'my-latex-mode-config)

;; Find TeX on Mac
(when *is-a-mac*
  (setenv "PATH" (concat (getenv "PATH") ":/Library/TeX/texbin/"))
  (setq exec-path (append exec-path '("/Library/TeX/texbin/"))))

(provide 'init-latex)
;;; init-latex.el ends here
