;;; init-recentf.el --- Settings for tracking recent files -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(add-hook 'after-init-hook 'recentf-mode)
(setq-default
 recentf-max-saved-items 100
 recentf-exclude '("/tmp/"))
;;"/ssh:"

(global-set-key "\C-x\ \C-r" 'recentf-open-files)

(setq save-silently t)
(after-load 'recentf
  (run-at-time nil (* 3 60) 'recentf-save-list))

(provide 'init-recentf)
;;; init-recentf.el ends here
