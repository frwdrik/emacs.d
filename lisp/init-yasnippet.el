;;; init-yasnippets.el --- Provide yasnippets functinos  -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require-package 'yasnippet)
(yas-global-mode 1)


(load (concat user-emacs-directory
	      (convert-standard-filename
	       "snippets/yasnippet-snippets/yasnippet-snippets.el")))

(setq yas-triggers-in-field t)

;(require 'yasnippet-snippets)
(provide 'init-yasnippet)
;;; init-yasnippet.el ends here
