;;; init-neotree.el --- Use neotree for sidebar navigation -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(when (maybe-require-package 'neotree)
  ; (add-hook 'after-init-hook 'neotree-show)

  (defun neotree-project-dir ()
    "Open NeoTree using the git root."
    (interactive)
    (let ((project-dir (projectile-project-root))
          (file-name (buffer-file-name)))
      (neotree-toggle)
      (if project-dir
          (if (neo-global--window-exists-p)
              (progn
		(neotree-dir project-dir)
		(neotree-find file-name)))
	(message "Could not find git project root."))))

  (global-set-key [f8] 'neotree-project-dir)

  (setq neo-smart-open t)
  )


(provide 'init-neotree)

;;; init-neotree.el ends here
