* Sunday, 28/07/19
** 19:53
In this "morning page" I will write a summary of Lehmann's expository *A snapshot of the minimal model program*. I will not discuss much about singularities.
- For varieties with torsion canonical line bundle, the fundamental group is almost abelian and rational curves are contained in a countable union of proper subsets.
*** Guiding conjecture of MMP: Any smooth X admits either
  1) a birational model X - - > X' and morphism X' -> Z with connected fibers to a variety of smaller dimension with general fiber a Fano.
  2) Like above, but general fiber has torsion canonical.
  3) Like above, but X' -> Z is birational and K_Z ample.

*** Questions:
  a) How can we identify the target Z, or equivalently, the map X - - > Z?
  b) How can we identify the rational map X - - > X'? What is the "right" birational model X'?
  c) How can we predict the cases 1, 2, 3 above from the geometry of X?

We start by looking at question a) for the cases 2) and 3). By BCHM, Corollary 1.1.2, the canonical section ring of a smooth variety is finitely generated. A multiple mK_X has sections only in the two cases we are considering, and we define the canonical model to be Proj of the ring. It is expected, but not proven, that this coincides with the map in question a) predicted by the MMP conjecture.

**** Remarks
- In the case 1), one has to add in a small multiple of an ample divisor. There is a lot more to say here.
- Having finite generation as above does not solve all our problems. First of all, it might be that no multiple of K_X has any sections (like for uniruled varieties). Secondly, we don't know if the map to the canonical model factors as a birational map X - - > X' followed by a fibration in pure types.


*** Obstructions from negativity
Let us identify the obstructions to the factorisation of rational maps as
X - - > X' -> Z, a birational map followed by a fibration in pure types. Let us assume, for simplicity, that the rational map X -> Proj R(X,K_X) is a morphism. Then there exists an ample divisor A on the target such that mK_X = pullback of A + some effective E on X. There are two cases:
1) If the dimension drops, then the canonical divisor on each fibre is the restriction of E to that fiber.
2) If the map is birational, then E is controlling the singularities of this canonical model.
In either case, the divisor E represents the obstruction to the conclusion of the MMP conjecture. So we are led to try removing the base locus of K_X, which means we first of all need to find a model X' on which K_X' is nef. If we could remove the base locus, then K_X' would define a morphism with fibers of pure type.

*** Question b)
How to  find the birational model X' of X? First we need to know whether we can contract K_X-negative curves. There is a positive answer to this, contained in the Cone Theorem. The birational contraction is either a divisorial contraction or a flip. There is a conjecture on the termination of flips.

The end result, assuming running the MMP terminates, is a birational model X'. This X' must either be a Mori Fiber Space or a minimal model. If it is a MFS, then it still has K_X-negative curves, but doing a contraction results in something of smaller dimension.

The termination of flips would imply the following:
Suppose X is not uniruled. Then X admits a minimal model.

Contrasting minimal and canonical models:
- Minimal models are expected to exist when X is not uniruled, and the canonical model exist when K_X has sections. By the abundance conjecture, these two conditions are expected to be equivalent.
- A minimal model is always birational to X, but in case 2), the canonical model has smaller dimension.
- There can be multiple minimal models, but only one canonical model.
- There should be a morphism from any minimal model of X to the canonical model of X.
- If X' is a minimal model of X, then K_X' may be zero on some curves, but (conjecturally) all such curves should be contracted by the morphism to the canonical model.

Let Z be the canonical model of X. Assuming the abundance conjecture, we have:
- In case 2), dim Z < dim X. The fibers are of Calabi-Yau type.
- In case 3), Z is birational to X. We can then show that K_Z is ample.

*** Tomorrow I will discuss more background and some key results.
